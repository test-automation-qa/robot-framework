*** Settings ***
Library    SeleniumLibrary

Test Teardown     Close All Browsers

*** Test Cases ***


open chrome browser for medium - Open Browser
    Open Browser    https://pradappandiyan.medium.com/    headlesschrome
    set window size     1280        800
    input text    //input[@placeholder="Search"]        "How to Handle OTP Verification on Test Automation"

open chrome browser for linkedln - Open Browser
    Open Browser    https://www.linkedin.com/in/pradap-pandiyan/    headlesschrome
    set window size     1280        800
    click element    //svg[@xmlns:xlink="http://www.w3.org/1999/xlink"]
